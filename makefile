PROGRAM=main
SRC=main.o lib.o dict.o

$(PROGRAM): $(SRC)
	$(LD) -o $@ $(SRC)

main.o: main.asm lib.inc words.inc dict.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean test

test: $(PROGRAM)
	python3 test.py

clean:
	rm -f $(PROGRAM) $(SRC)
