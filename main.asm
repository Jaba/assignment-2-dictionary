%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define buff_size 256
%define err_exit_code 1
%define std_out 1
%define std_err 2
%define WORD 8

section .bss
buffer: resb buff_size

section .rodata:
    error_not_found: db "Not found this word",10,0
    error_too_long: db "This word is too long",10,0 

section .text

global _start

_start: 
    mov rdi, buffer
    ;убрано "магическое" число в соответствии с комменатрием
    mov rsi, buff_size
    call read_word
    test rax,rax
    jz .long_err
    mov rsi, first_word
    mov rdi,buffer
    call find_word
    test rax,rax
    jz .found_err
    mov rdi,rax
    ;убрано магическое число
    add rdi,WORD
    push rdi
    call string_length
    pop rdi
    add rdi,rax
    inc rdi
    call print_string
    call exit

.found_err:
    mov rdi, error_not_found
    jmp .print_err
.long_err:
    mov rdi,error_too_long
    jmp .print_err
.print_err:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax,std_out
    mov rdi,std_err
    syscall
    call exit
