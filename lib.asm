section .data
%define newLine 0xA
%define exit_id 60
%define writeSyscallNumber 1
%define stdoutDescriptor 1
%define null_termin 0
%define tabulation 0x9
%define space 0x20
%define out_syscall 0
%define stdin_descriptor 0 
section .text
 
global exit
global print_string
global string_length
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, exit_id 
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину

print_string:

    xor rax,rax
    push rdi

    call string_length
    
    pop rsi;fixed: можно просто pop rsi, тогда строчка mov rsi, rdi не нужна, также магические константы, которые записываешь в rax и rdi перед syscall

    mov rdx, rax        
    mov rax, writeSyscallNumber
    mov rdi, stdoutDescriptor
    syscall

    ret

string_length:
    ; Вход: rdi - Входная строка
    ; Выход: rax - вернет длину строки
    xor rax, rax 

    .loop:
        cmp byte [rax+rdi], 0  ; Проверка на нуль-терминатор
        je .end
        
        inc rax
        jmp .loop
    .end:
        ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi

    mov rax, writeSyscallNumber
    mov rdi, stdoutDescriptor
    mov rsi, rsp
    mov rdx, 1; 1 байт для вывода. Не думаю, что имеет смысл выносить ее в отдельную переменную

    syscall

    pop rdi 

    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:

    mov rdi,newLine
    jmp print_char

    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    
    xor rax, rax
    xor rcx, rcx
    
    mov r8,10

    mov rax, rdi

    dec rsp;зарезервировали место на стеке
    mov byte[rsp], 0

    inc rcx
    .loop:
        xor rdx, rdx

        inc rcx
        div r8 
        add rdx, '0' 

        dec rsp
        mov byte[rsp], dl;младшие 8 бит регистра rdx -> byte[rsp]
        test rax, rax
        jnz .loop

    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    add rsp, rcx
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    js .sign
    jmp print_uint

    .sign:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx,rcx
    
    .loop:
        mov al, byte[rdi]
        cmp al, byte[rsi]
        ; test al,byte
        ; cmp al, cl
        jnz .end

        inc rdi
        inc rsi
        ; test cl, cl
        cmp al, null_termin
        jnz .loop
        mov rax, 1
        
        ret 
    .end:
        mov rax,0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi,rsp

    mov rdx,1
    mov rdi, stdin_descriptor
    mov rax,out_syscall
    syscall

    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12                           
    push r13                            
    push r14
    push rdi
                                
    mov r12, rdi
    mov r13, rsi
    mov r14, rcx

    .space_skip:
        call read_char
        cmp rax, space          
        je .space_skip
        cmp rax, tabulation
        je .space_skip
        cmp rax, newLine    
        je .space_skip
        xor rcx, rcx        
                
    .loop:                             
        inc r14                        
        cmp r14, r13                   
        jg .overflow                    
        mov byte[r12], al               
        cmp rax, 0                     
        je .success
        cmp rax, space
        je .success
        cmp rax, tabulation
        je .success
        cmp rax, newLine
        je .success
        inc r12                         
        call read_char              
        jmp .loop

    .overflow:
        mov rcx, r14
        dec rcx                         
        pop rdi                        
        mov rax, 0                     
        jmp .end

    .success:
        mov rdx, r14
        dec rdx                        
        pop rax                         
    .end:
        pop r14
        pop r13
        pop r12
        ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    ;rdx - длинна числа
    ;rcx - буфер

    xor rax, rax		   
	xor rdx, rdx		    
	xor rcx, rcx	

    mov r11, 10		    
	.loop:
	    xor rcx, rcx        
  	    mov cl, [rdi+rdx]	
	    cmp cl, '0'		    
	    jb .end 
	    cmp cl, '9'         
	    ja .end
	    sub rcx, '0'		

	    ; mov r11, 10		    
	    push rdx            
	    mul r11             
	    pop rdx             

        ;rax = rax * 10 + rcx
	    add rax, rcx	    
	    inc rdx            
	    jmp .loop
	
	.end:
 	    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov cl, byte[rdi]                  
    cmp cl, '-'                       
    je .leave_sign

    cmp cl, '+'                         
    je .leave_sign
    
    jmp .parse
    .leave_sign:
        inc rdi                         
    .parse:
        push rcx                        
        call parse_uint                
        pop rcx                        
        cmp cl, '-'                     
        jne .end                        
        neg rax                         
        inc rdx                       
    .end:
        ret 



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
     xor rcx, rcx

    .loop:
        mov al, byte[rdi]
        inc rdi                         
        inc rcx                         
        cmp rcx, rdx                    
        jg .fail     

        mov byte[rsi], al               
        inc rsi
        ;правка внесена, было замененно cmp al,0 на test al,al
        test al, al                     
        je .success
        
        jmp .loop
    .fail:
        mov rax, 0                     
        ret
    .success:
        mov rax, rcx  
        ret                  
