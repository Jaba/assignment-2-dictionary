;macros for auto adding new element of the dictionary to the head of it 
;first arg must be a key and it must be a string
;will drop error if first arg not string
;
;second arg is id for the nextElement
%define nextElement 0

%macro colon 2 
    %ifnstr %1
        %error "Key must be a string"
    %endif
    %ifnid %2
        %error "Id is invalid"
    %endif
    %2: 
     dq nextElement
        db %1
        db 0

%define nextElement %2
%endmacro
