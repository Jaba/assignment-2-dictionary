from subprocess import Popen, PIPE

input = ["data_first","second_data","some_data","no output","longStringForError"*300]

output = ["first_word", "second_word", "third_word3","",""]

errors = ["","","","Not found this word","This word is too long"]

try:
    for i in range(len(input)):
      p = Popen(["./main"],
            stdin=PIPE,
            stdout=PIPE,
            stderr=PIPE)
      inp = input[i]
      out = output[i]
      err = errors[i]
      data = p.communicate(inp.encode())
      if data[0].decode().strip() == out and data[1].decode().strip() == err:
            print("test №" + str(i+1) + " succeeded " + "with "
                                                       "output data: " + out + "\n")
      else:
        print("test №" + str(i+1) + " failed =>" + " Error: " + err + "\n")

except FileNotFoundError:
    print("some error with opening file")