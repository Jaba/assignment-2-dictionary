section .data

%define size_pointr 8

section .text
global find_word

%include "lib.inc"

find_word:
    .loop:
        test rsi, rsi
        jz .no_match

        ; Сохраняем rsi в rdx
        mov rdx, rsi

        add rsi, size_pointr
        call string_equals

        ; Восстанавливаем rsi из rdx
        mov rsi, rdx

        cmp rax, 1
        je .match

        mov rsi, [rsi] 
        jmp .loop

    .match:
        add rsi, size_pointr
        mov rax, rsi
        ret

    .no_match:
        xor rax, rax
        ret
